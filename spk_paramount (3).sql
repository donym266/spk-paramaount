-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 11, 2021 at 12:09 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `spk_paramount`
--

-- --------------------------------------------------------

--
-- Table structure for table `bagian`
--

CREATE TABLE `bagian` (
  `id_bagian` int(3) NOT NULL,
  `namaBagian` varchar(30) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bagian`
--

INSERT INTO `bagian` (`id_bagian`, `namaBagian`, `created_date`) VALUES
(1, 'Bagian Pemasaran', '2021-10-05 03:22:59');

-- --------------------------------------------------------

--
-- Table structure for table `bobot_kriteria`
--

CREATE TABLE `bobot_kriteria` (
  `id_bobotkriteria` int(3) NOT NULL,
  `id_bagian` int(3) NOT NULL,
  `id_kriteria` int(3) NOT NULL,
  `bobot` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bobot_kriteria`
--

INSERT INTO `bobot_kriteria` (`id_bobotkriteria`, `id_bagian`, `id_kriteria`, `bobot`) VALUES
(7, 1, 1, 0.5),
(8, 1, 2, 1),
(9, 1, 3, 0.75),
(10, 1, 4, 1),
(11, 1, 5, 1),
(12, 1, 6, 0.5);

-- --------------------------------------------------------

--
-- Table structure for table `hasil`
--

CREATE TABLE `hasil` (
  `id_hasil` int(3) NOT NULL,
  `id_bagian` int(3) NOT NULL,
  `id_pegawai` int(3) NOT NULL,
  `hasil` float NOT NULL,
  `tahun` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hasil`
--

INSERT INTO `hasil` (`id_hasil`, `id_bagian`, `id_pegawai`, `hasil`, `tahun`) VALUES
(1, 1, 6, 3.8335, 0),
(2, 1, 7, 4, 0),
(3, 1, 8, 3.75025, 0),
(4, 1, 0, 0, 0),
(5, 1, 6, 3.8335, 2021),
(6, 1, 7, 4, 2021),
(7, 1, 8, 3.75025, 2021);

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE `kriteria` (
  `id_kriteria` int(3) NOT NULL,
  `namaKriteria` varchar(30) NOT NULL,
  `sifat` enum('Benefit','Cost') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`id_kriteria`, `namaKriteria`, `sifat`) VALUES
(1, 'Kecerdasan', 'Cost'),
(2, 'Semangan Berkerja', 'Benefit'),
(3, 'Kerja Sama', 'Benefit'),
(4, 'Disiplin Kerja', 'Benefit'),
(5, 'Keterampilan', 'Benefit'),
(6, 'Kepemimpinan', 'Benefit');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_kriteria`
--

CREATE TABLE `nilai_kriteria` (
  `id_nilaikriteria` int(3) NOT NULL,
  `id_kriteria` int(3) NOT NULL,
  `nilai` float NOT NULL,
  `keterangan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_kriteria`
--

INSERT INTO `nilai_kriteria` (`id_nilaikriteria`, `id_kriteria`, `nilai`, `keterangan`) VALUES
(5, 2, 0.25, 'Kurang'),
(6, 2, 0.5, 'Biasa'),
(7, 2, 0.75, 'Bagus'),
(8, 2, 1, 'Bagus Sekali'),
(10, 3, 0, 'Tidak Bagus'),
(11, 3, 0.25, 'Kurang'),
(12, 3, 0.5, 'Biasa'),
(13, 3, 0.75, 'Bagus'),
(14, 4, 0.25, 'Kurang'),
(15, 4, 0.5, 'Biasa'),
(16, 4, 0.75, 'Bagus'),
(17, 5, 0.5, 'Biasa'),
(18, 5, 1, 'Bagus Sekali'),
(19, 6, 0.25, 'Kurang'),
(20, 6, 0.5, 'Biasa'),
(21, 6, 0.75, 'Bagus'),
(22, 1, 0.25, 'Tidak Bagus'),
(23, 1, 0.5, 'Kurang'),
(24, 1, 0.75, 'Biasa'),
(25, 1, 1, 'Bagus'),
(26, 3, 1, 'Bagus Sekali'),
(27, 4, 1, 'Bagus Sekali'),
(28, 6, 1, 'Bagus Sekali');

-- --------------------------------------------------------

--
-- Table structure for table `nilai_pegawai`
--

CREATE TABLE `nilai_pegawai` (
  `id_nilaipegawai` int(3) NOT NULL,
  `id_pegawai` int(3) NOT NULL,
  `id_bagian` int(3) NOT NULL,
  `id_kriteria` int(3) NOT NULL,
  `id_nilaikriteria` int(3) NOT NULL,
  `tahun` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai_pegawai`
--

INSERT INTO `nilai_pegawai` (`id_nilaipegawai`, `id_pegawai`, `id_bagian`, `id_kriteria`, `id_nilaikriteria`, `tahun`) VALUES
(19, 6, 1, 1, 23, 2021),
(20, 6, 1, 2, 6, 2021),
(21, 6, 1, 3, 13, 2021),
(22, 6, 1, 4, 16, 2021),
(23, 6, 1, 5, 18, 2021),
(24, 6, 1, 6, 20, 2021),
(25, 7, 1, 1, 23, 2021),
(26, 7, 1, 2, 7, 2021),
(27, 7, 1, 3, 13, 2021),
(28, 7, 1, 4, 15, 2021),
(29, 7, 1, 5, 18, 2021),
(30, 7, 1, 6, 21, 2021),
(31, 8, 1, 1, 25, 2021),
(32, 8, 1, 2, 8, 2021),
(33, 8, 1, 3, 12, 2021),
(34, 8, 1, 4, 27, 2021),
(35, 8, 1, 5, 17, 2021),
(36, 8, 1, 6, 21, 2021);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE `pegawai` (
  `id_pegawai` int(3) NOT NULL,
  `id_bagian` int(10) NOT NULL,
  `namaPegawai` varchar(30) NOT NULL,
  `nikPegawai` varchar(100) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `tanggal_lahir` varchar(100) NOT NULL,
  `gambar` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `tempat_lahir` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`id_pegawai`, `id_bagian`, `namaPegawai`, `nikPegawai`, `alamat`, `tanggal_lahir`, `gambar`, `created_date`, `tempat_lahir`) VALUES
(6, 1, 'Banu', '123', '', '', '', '2021-10-05 03:19:51', ''),
(7, 1, 'Gugun', '456', '', '', '', '2021-10-05 03:19:51', ''),
(8, 1, 'Jajang', '789', '', '', '', '2021-10-05 03:19:51', ''),
(50, 0, 'dony marulloh3', '2111111111', 'depok 2', '25 juni 2000', 'u7.jpg', '2021-10-10 07:00:10', 'Depok');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_admin` int(3) NOT NULL,
  `username` varchar(30) NOT NULL,
  `nama_lengkap` text NOT NULL,
  `nik` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `role` int(1) NOT NULL DEFAULT 1,
  `id_bagian` int(10) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_admin`, `username`, `nama_lengkap`, `nik`, `password`, `role`, `id_bagian`, `created_date`) VALUES
(1, 'admin', 'admin', '', 'password', 0, 0, '2021-10-05 03:22:35'),
(2, 'joko', 'Joko Anwar', '1234243', 'password', 1, 1, '2021-10-08 14:38:02'),
(3, 'joko1', 'joko budi', '', 'password', 1, 1, '2021-10-08 17:40:39');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bagian`
--
ALTER TABLE `bagian`
  ADD PRIMARY KEY (`id_bagian`);

--
-- Indexes for table `bobot_kriteria`
--
ALTER TABLE `bobot_kriteria`
  ADD PRIMARY KEY (`id_bobotkriteria`);

--
-- Indexes for table `hasil`
--
ALTER TABLE `hasil`
  ADD PRIMARY KEY (`id_hasil`);

--
-- Indexes for table `kriteria`
--
ALTER TABLE `kriteria`
  ADD PRIMARY KEY (`id_kriteria`);

--
-- Indexes for table `nilai_kriteria`
--
ALTER TABLE `nilai_kriteria`
  ADD PRIMARY KEY (`id_nilaikriteria`);

--
-- Indexes for table `nilai_pegawai`
--
ALTER TABLE `nilai_pegawai`
  ADD PRIMARY KEY (`id_nilaipegawai`);

--
-- Indexes for table `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_admin`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bagian`
--
ALTER TABLE `bagian`
  MODIFY `id_bagian` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `bobot_kriteria`
--
ALTER TABLE `bobot_kriteria`
  MODIFY `id_bobotkriteria` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `hasil`
--
ALTER TABLE `hasil`
  MODIFY `id_hasil` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `kriteria`
--
ALTER TABLE `kriteria`
  MODIFY `id_kriteria` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `nilai_kriteria`
--
ALTER TABLE `nilai_kriteria`
  MODIFY `id_nilaikriteria` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `nilai_pegawai`
--
ALTER TABLE `nilai_pegawai`
  MODIFY `id_nilaipegawai` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `id_pegawai` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_admin` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
