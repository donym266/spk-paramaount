<div class="page-heading">
    <h1 class="page-title">Penilaian</h1>
</div>
<div class="page-content fade-in-up">
    <div class="ibox">
        <div class="ibox-head">
            <b style="float: left">Daftar Penilaian</b>
            <div style="float:right;">
                <select class="form-control-sm"  name="pilihNilai" id="pilihNilai">
                    <option value="">Semua Devisi/Bagian</option>;
                    <?php
                    $query = "SELECT * FROM bagian";
                    $execute = $konek->query($query);
                    if ($execute->num_rows > 0) {
                        while ($data = $execute->fetch_array(MYSQLI_ASSOC)) {
                            if ($pilih == $data[id_kriteria]) {
                                $selected = "selected";
                            } else {
                                $selected = null;
                            }
                            echo "<option $selected value=$data[id_bagian]>$data[namaBagian]</option>";
                        }
                    } else {
                        echo '<option disabled value="">Tidak ada data</option>';
                    }
                    ?>
                </select>
            </div>
        </div>
        <div class="ibox-body">
            <table class="table table-striped table-bordered table-hover" id="sub-kriteria-table" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>NIK</th>
                        <th>Bagian</th>
                        <th>Tahun</th>

                        <th>Aksi</th>

                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>No</th>
                        <th>Nama</th>
                        <th>NIK</th>
                        <th>Bagian</th>
                        <th>Tahun</th>

                        <th>Aksi</th>
                    </tr>
                </tfoot>
                <tbody id="isiNilai"></tbody>
            </table>
        </div>
    </div>
</div>
